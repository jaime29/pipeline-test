CHAT_CHANNEL=

CHAT_INPUT=

CI=true

CI_API_V4_URL=https://gitlab.com/api/v4

CI_BUILDS_DIR=/builds

CI_COMMIT_BEFORE_SHA=0000000000000000000000000000000000000000

CI_COMMIT_DESCRIPTION=

CI_COMMIT_MESSAGE=Add .gitlab-ci.yml

CI_COMMIT_REF_NAME=master

CI_COMMIT_REF_PROTECTED=true

CI_COMMIT_REF_SLUG=master

CI_COMMIT_SHA=1c88a20914dfe0796cbbcf06fd071d19fa8f25d0

CI_COMMIT_SHORT_SHA=1c88a209

CI_COMMIT_BRANCH=master

CI_COMMIT_TAG=

CI_COMMIT_TITLE=Add .gitlab-ci.yml

CI_COMMIT_TIMESTAMP=2021-02-13T00:15:18+00:00

CI_CONCURRENT_ID=87

CI_CONCURRENT_PROJECT_ID=0

CI_CONFIG_PATH=.gitlab-ci.yml

CI_DEBUG_TRACE=

CI_DEFAULT_BRANCH=master

CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX=gitlab.com:443/jaime29/dependency_proxy/containers

CI_DEPENDENCY_PROXY_SERVER=gitlab.com:443

CI_DEPENDENCY_PROXY_PASSWORD=xxxxxxxxxxxxxxxxxxxx

CI_DEPENDENCY_PROXY_USER=gitlab-ci-token

CI_DEPLOY_FREEZE=

CI_DEPLOY_PASSWORD=

CI_DEPLOY_USER=

CI_DISPOSABLE_ENVIRONMENT=true

CI_ENVIRONMENT_NAME=

CI_ENVIRONMENT_SLUG=

CI_ENVIRONMENT_URL=

CI_EXTERNAL_PULL_REQUEST_IID=

CI_EXTERNAL_PULL_REQUEST_SOURCE_REPOSITORY=

CI_EXTERNAL_PULL_REQUEST_TARGET_REPOSITORY=

CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_NAME=

CI_EXTERNAL_PULL_REQUEST_SOURCE_BRANCH_SHA=

CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_NAME=

CI_EXTERNAL_PULL_REQUEST_TARGET_BRANCH_SHA=

CI_HAS_OPEN_REQUIREMENTS=

CI_OPEN_MERGE_REQUESTS=

CI_JOB_ID=1027787966

CI_JOB_IMAGE=

CI_JOB_MANUAL=

CI_JOB_NAME=echo-vars

CI_JOB_STAGE=test

CI_JOB_STATUS=running

CI_JOB_TOKEN=xxxxxxxxxxxxxxxxxxxx

CI_JOB_JWT=eyJhbGciOiJSUzI1NiIsImtpZCI6IjRpM3NGRTdzeHFOUE9UN0ZkdmNHQTFaVkdHSV9yLXRzRFhuRXVZVDRacUUiLCJ0eXAiOiJKV1QifQ.eyJuYW1lc3BhY2VfaWQiOiI4Nzc1ODQxIiwibmFtZXNwYWNlX3BhdGgiOiJqYWltZTI5IiwicHJvamVjdF9pZCI6IjI0Mzc3NzI1IiwicHJvamVjdF9wYXRoIjoiamFpbWUyOS9waXBlbGluZS10ZXN0IiwidXNlcl9pZCI6IjY1ODYzMzkiLCJ1c2VyX2xvZ2luIjoiamFpbWUyOSIsInVzZXJfZW1haWwiOiJqYWltZUByb3V0ZS5jb20iLCJwaXBlbGluZV9pZCI6IjI1NTcwMzkzNCIsImpvYl9pZCI6IjEwMjc3ODc5NjYiLCJyZWYiOiJtYXN0ZXIiLCJyZWZfdHlwZSI6ImJyYW5jaCIsInJlZl9wcm90ZWN0ZWQiOiJ0cnVlIiwianRpIjoiZDAzZGM4MGItMjU5Mi00Nzg2LWFlNzQtMTgxNzEwZWQzZDcxIiwiaXNzIjoiZ2l0bGFiLmNvbSIsImlhdCI6MTYxMzE3NTMyMCwibmJmIjoxNjEzMTc1MzE1LCJleHAiOjE2MTMxNzg5MjAsInN1YiI6ImpvYl8xMDI3Nzg3OTY2In0.LNFPBKL7fuxk6KHWSsYJw9MJQWrVq5yTD9zxqn4XxvYVPsV9Uv_LnpaW2hoRjW_LUsNQ9XuJzpDtlX0c5oBH_RKCfS-OvCyzmapiazlxrHIu1NmzHlKVXZIEf6Se2Jlttx2frJxsXhT5vL7ygkW7tEe1dVcFzTWafzxTjvBgF_muwKlsg46kPU3CD7GJz_enXZGWcXrEUvaNx0XKhGQ9ta5ndBTpr0iAg8ooSktKWUux-L0ykSgCTkHfNPCUVl_Yp1L6ntnBlqaxD5lPYamjqPP2eAEML74xS8hAL-FhMwO0YfSuCYro91GQmR4vJnUwuQYsNeg3Fu_QEg-IUE-Lyw

CI_JOB_URL=https://gitlab.com/jaime29/pipeline-test/-/jobs/1027787966

CI_KUBERNETES_ACTIVE=

CI_MERGE_REQUEST_ASSIGNEES=

CI_MERGE_REQUEST_ID=

CI_MERGE_REQUEST_IID=

CI_MERGE_REQUEST_LABELS=

CI_MERGE_REQUEST_MILESTONE=

CI_MERGE_REQUEST_PROJECT_ID=

CI_MERGE_REQUEST_PROJECT_PATH=

CI_MERGE_REQUEST_PROJECT_URL=

CI_MERGE_REQUEST_REF_PATH=

CI_MERGE_REQUEST_SOURCE_BRANCH_NAME=

CI_MERGE_REQUEST_SOURCE_BRANCH_SHA=

CI_MERGE_REQUEST_SOURCE_PROJECT_ID=

CI_MERGE_REQUEST_SOURCE_PROJECT_PATH=

CI_MERGE_REQUEST_SOURCE_PROJECT_URL=

CI_MERGE_REQUEST_TARGET_BRANCH_NAME=

CI_MERGE_REQUEST_TARGET_BRANCH_SHA=

CI_MERGE_REQUEST_TITLE=

CI_MERGE_REQUEST_EVENT_TYPE=

CI_MERGE_REQUEST_DIFF_ID=

CI_MERGE_REQUEST_DIFF_BASE_SHA=

CI_NODE_INDEX=

CI_NODE_TOTAL=1

CI_PAGES_DOMAIN=gitlab.io

CI_PAGES_URL=https://jaime29.gitlab.io/pipeline-test

CI_PIPELINE_ID=255703934

CI_PIPELINE_IID=1

CI_PIPELINE_SOURCE=push

CI_PIPELINE_TRIGGERED=

CI_PIPELINE_URL=https://gitlab.com/jaime29/pipeline-test/-/pipelines/255703934

CI_PROJECT_CONFIG_PATH=.gitlab-ci.yml

CI_PROJECT_DIR=/builds/jaime29/pipeline-test

CI_PROJECT_ID=24377725

CI_PROJECT_NAME=pipeline-test

CI_PROJECT_NAMESPACE=jaime29

CI_PROJECT_ROOT_NAMESPACE=jaime29

CI_PROJECT_PATH=jaime29/pipeline-test

CI_PROJECT_PATH_SLUG=jaime29-pipeline-test

CI_PROJECT_REPOSITORY_LANGUAGES=

CI_PROJECT_TITLE=pipeline-test

CI_PROJECT_URL=https://gitlab.com/jaime29/pipeline-test

CI_PROJECT_VISIBILITY=private

CI_REGISTRY=registry.gitlab.com

CI_REGISTRY_IMAGE=registry.gitlab.com/jaime29/pipeline-test

CI_REGISTRY_PASSWORD=xxxxxxxxxxxxxxxxxxxx

CI_REGISTRY_USER=gitlab-ci-token

CI_REPOSITORY_URL=https://gitlab-ci-token:xxxxxxxxxxxxxxxxxxxx@gitlab.com/jaime29/pipeline-test.git

CI_RUNNER_DESCRIPTION=shared-runners-manager-4.gitlab.com

CI_RUNNER_EXECUTABLE_ARCH=linux/amd64

CI_RUNNER_ID=44949

CI_RUNNER_REVISION=d8cb88a0

CI_RUNNER_SHORT_TOKEN=72989761

CI_RUNNER_TAGS=gce, east-c, shared, docker, linux, ruby, mysql, postgres, mongo, git-annex

CI_RUNNER_VERSION=13.9.0-rc1

CI_SERVER=yes

CI_SERVER_URL=https://gitlab.com

CI_SERVER_HOST=gitlab.com

CI_SERVER_PORT=443

CI_SERVER_PROTOCOL=https

CI_SERVER_NAME=GitLab

CI_SERVER_REVISION=8cb7781ba75

CI_SERVER_VERSION=13.9.0-pre

CI_SERVER_VERSION_MAJOR=13

CI_SERVER_VERSION_MINOR=9

CI_SERVER_VERSION_PATCH=0

CI_SHARED_ENVIRONMENT=

GITLAB_CI=true

GITLAB_FEATURES=elastic_search,ldap_group_sync,multiple_ldap_servers,repository_size_limit,seat_link,usage_quotas,admin_audit_log,auditor_user,custom_file_templates,custom_project_templates,db_load_balancing,default_branch_protection_restriction_in_groups,extended_audit_events,external_authorization_service_api_management,geo,ldap_group_sync_filter,object_storage,pages_size_limit,project_aliases,required_ci_templates,enterprise_templates

GITLAB_USER_EMAIL=jaime@route.com

GITLAB_USER_ID=6586339

GITLAB_USER_LOGIN=jaime29

GITLAB_USER_NAME=Jaime Steren
